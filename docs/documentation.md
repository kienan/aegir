Documentation
=============

The Ægir documentation site is generated with [mkdocs](http://www.mkdocs.org/). To get started contributing to this project, fork it on GitLab. Then install mkdocs and clone this repo locally:

    :::console
    $ sudo brew install python              # For OSX users
    $ sudo aptitude install python-pip      # For Debian/Ubuntu users
    $ sudo pip install mkdocs
    $ git clone https://gitlab.com/aegir/aegir
    $ cd aegir
    $ git remote add sandbox https://gitlab.com/<username>/aegir   # URL for your fork
    $ mkdocs build --clean
    $ mkdocs serve

Your local Ægir docs site should now be available for browsing: [http://127.0.0.1:42000/](http://127.0.0.1:42000/).

When you find a typo, an error, unclear or missing explanations or instructions, open a new terminal and start editing. Your changes should be reflected automatically on the local server. Find the page you’d like to edit; everything is in the docs/ directory. Make your changes, commit and push them, and start a pull request:

    :::console
    $ git checkout -b fix_typo
    $ vim docs/index.md                     # Add/edit/remove whatever you see fit. Be bold!
    $ mkdocs build --clean; mkdocs serve    # Go check your changes. We’ll wait...
    $ git diff                              # Make sure there aren’t any unintended changes.
    diff --git a/docs/index.md b/docs/index.md
    ...
    $ git commit -am”Fixed typo.”           # Useful commit message are a good habit.
    $ git push sandbox fix_typo

Visit your fork on Gitlab and start a Merge Request.
