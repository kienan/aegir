Developing Ægir
===============

Ægir is built on Ansible and Drupal 8. At this point, it consists mostly of a custom Ansible role, and Drupal modules exported using Features.


Dependencies
------------

The various components of Ægir are written in different languages and frameworks, which entails different methods for managing dependencies and requirements. Here are the most notable mechanisms we use to manage these:

* `build/ansible/requirements.yml`: Ansible roles.
* `build/ansible/roles/aegir.aegir/meta/main.yml`: Ansible dependencies.
* `profile/composer.json`: Drupal profile PHP dependencies.
* `profile/aegir.info.yml`: Drupal dependencies.
* `build/ansible/roles/aegir.aegir/files/platform_build.j2`: Template to generate a composer.json that will build a Drupal platform for Aegir. Includes `drush`, `behat`, `phpcs`, etc.
* `make ansible`: Standardized method for installing Ansible (via Drumkit).

