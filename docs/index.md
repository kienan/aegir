Welcome to Ægir
===============

Ægir is a framework for hosting and managing websites and other applications.

Ægir is built as a Drupal distribution, making it both easy to self-host, integrate with other tools and services, and extend with plugins, themes or custom code.

The version documented here is a full re-write using Drupal 8. It is not yet functional, and [Aegir 3.x](http://docs.aegirproject.org) remains the supported release for the foreseeable future.

Check out [http://aegir.hosting/](http://aegir.hosting/) for more information. Professional support, training, consulting and other services are offered by the [Ægir Cooperative](http://aegir.coop).

This documentation is designed primarily for the following audiences:

* [Users](usage.md): those looking to use Ægir to install and manage a website or other supported application.
* [Administrators](administration.md): those looking to host Ægir on their own infrastructure.
* [Developers](development.md): those looking to add new features, enhance or fix bugs in existing ones or otherwise extend Ægir.
