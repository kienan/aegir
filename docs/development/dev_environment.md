Development Environment
=======================

To get started, clone this repo locally, and check out the feature branch you want to work on:

    :::console
    $ git clone --recursive https://gitlab.com/aegir/aegir.git
    $ cd aegir
    $ git checkout 6-base-profile-feature

Bootstrap Drumkit, and install a local development site:

    :::console
    $ . d    # Note the *space*, as we're **sourcing** this file, in order to load some environment variable, etc.
    $ make drupal

You should now have a browser window open on the development site (http://localhost:8888/) prompting you to login using the one-time login link.

