if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
  echo "This script must be sourced. Try \". ${BASH_SOURCE[0]}\"" 1>&2
  exit 1
fi

git submodule update --init

CWD=`pwd`
LOCAL_DIR=${LOCAL_DIR:-$CWD/.mk/.local}

export PATH="$LOCAL_DIR/bin:$PATH"

