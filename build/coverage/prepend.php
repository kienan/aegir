<?php

use SebastianBergmann\CodeCoverage\CodeCoverage;
use SebastianBergmann\CodeCoverage\Report\Html\Facade;
use SebastianBergmann\CodeCoverage\Report\Text;
use SebastianBergmann\CodeCoverage\Report\PHP;

code_coverage_before();

function code_coverage_before() {
  global $coverage;

  $aegir_platform_path = getenv('AEGIR_PLATFORM_PATH') ? getenv('AEGIR_PLATFORM_PATH') : '/var/aegir/platforms/aegir';

  require_once $aegir_platform_path . '/vendor/autoload.php';

  $coverage = new CodeCoverage();
  #$coverage->setDisableIgnoredLines(TRUE);
  $coverage->setCheckForUnexecutedCoveredCode(TRUE);
  $coverage->setAddUncoveredFilesFromWhitelist(TRUE);
  #$coverage->setProcessUncoveredFilesFromWhitelist(TRUE);

  $profile_path = $aegir_platform_path . '/profiles/aegir';
  $suffixes = ['.php', '.module', '.install', '.inc', '.theme'];
  foreach ($suffixes as $suffix) {
    $coverage->filter()->addDirectoryToWhitelist($profile_path, $suffix);
  }

  register_shutdown_function('code_coverage_after');

  $coverage->start('aegir');
}

/**
 * Generate a uniquely-named PHP-serialized report.
 *
 * We then merge these later via `phpcov merge`.
 */
function code_coverage_after() {
  global $coverage;

  $coverage->stop();

  $project_dir = getenv('CI_PROJECT_DIR') ? getenv('CI_PROJECT_DIR') : '/vagrant';
  $php_report_path = $project_dir . '/build/coverage/php';
  $php_report_file = $php_report_path . '/index-' . uniqid() . '.cov';

  $result = new PHP();
  $result->process($coverage, $php_report_file);
}
