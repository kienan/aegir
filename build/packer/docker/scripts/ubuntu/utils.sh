#!/bin/sh -eux

# Install some basic utilities
apt-get install -y -qq git make sudo curl unzip netcat > /dev/null
