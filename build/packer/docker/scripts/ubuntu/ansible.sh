#!/bin/sh -eux

# Install the latest version of Ansible.
apt-get -y install python-pip build-essential libssl-dev libffi-dev python-dev > /dev/null
pip install --upgrade pip > /dev/null
pip install setuptools > /dev/null
pip install ansible > /dev/null

