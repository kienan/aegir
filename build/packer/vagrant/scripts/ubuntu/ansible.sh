#!/bin/sh -eux

# Install the latest version of Ansible.
apt-get -y install python-pip;
pip install ansible

