Welcome to Ægir
===============

[![build status](https://gitlab.com/aegir/aegir/badges/master/build.svg)](https://gitlab.com/aegir/aegir/pipelines) [![coverage report](https://gitlab.com/aegir/aegir/badges/master/coverage.svg)](http://docs.aegir.hosting/coverage/index.html)

Ægir is a framework for hosting and managing websites and other applications.

Ægir is built as a Drupal distribution, making it both easy to self-host, integrate with other tools and services, and extend with plugins, themes or custom code.

Check out [http://aegir.hosting/](http://aegir.hosting/) for more information. Professional support, training, consulting and other services are offered by the [Ægir Cooperative](http://aegir.coop).

Technical documentation can be found at [http://docs.aegir.hosting](http://docs.aegir.hosting). This documentation is also available in the `docs/` directory.
