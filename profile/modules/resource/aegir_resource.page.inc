<?php

/**
 * @file
 * Contains aegir_resource.page.inc.
 *
 * Page callback for Ægir Resource entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Ægir resource templates.
 *
 * Default template: aegir_resource.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_aegir_resource(array &$variables) {
  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
