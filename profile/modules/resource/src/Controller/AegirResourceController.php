<?php

namespace Drupal\aegir_resource\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\aegir_resource\Entity\AegirResourceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AegirResourceController.
 *
 *  Returns responses for Ægir Resource routes.
 *
 * @package Drupal\aegir_resource\Controller
 */
class AegirResourceController extends ControllerBase {

  /**
   * An HTML renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * A date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs an AegirResourceController object.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   An HTML renderer.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   A date formatter.
   */
  public function __construct(
    RendererInterface $renderer,
    DateFormatterInterface $date_formatter
  ) {
    $this->renderer = $renderer;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('renderer'),
      $container->get('date.formatter')
    );
  }

  /**
   * Displays a Ægir Resource  revision.
   *
   * @param int $aegir_resource_revision
   *   The Ægir Resource  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($aegir_resource_revision) {
    $aegir_resource = $this->entityManager()->getStorage('aegir_resource')->loadRevision($aegir_resource_revision);
    $view_builder = $this->entityManager()->getViewBuilder('aegir_resource');

    return $view_builder->view($aegir_resource);
  }

  /**
   * Page title callback for a Ægir Resource  revision.
   *
   * @param int $aegir_resource_revision
   *   The Ægir Resource  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($aegir_resource_revision) {
    $aegir_resource = $this->entityManager()->getStorage('aegir_resource')->loadRevision($aegir_resource_revision);
    return $this->t('Revision of %title from %date', array('%title' => $aegir_resource->label(), '%date' => $this->dateFormatter->format($aegir_resource->getRevisionCreationTime())));
  }

  /**
   * Generates an overview table of older revisions of a Ægir Resource .
   *
   * @param \Drupal\aegir_resource\Entity\AegirResourceInterface $aegir_resource
   *   A Ægir Resource  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(AegirResourceInterface $aegir_resource) {
    $account = $this->currentUser();
    $langcode = $aegir_resource->language()->getId();
    $langname = $aegir_resource->language()->getName();
    $languages = $aegir_resource->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $aegir_resource_storage = $this->entityManager()->getStorage('aegir_resource');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $aegir_resource->label()]) : $this->t('Revisions for %title', ['%title' => $aegir_resource->label()]);
    $header = array($this->t('Revision'), $this->t('Operations'));

    $revert_permission = (($account->hasPermission("revert all aegir resource revisions") || $account->hasPermission('administer aegir resource entities')));
    $delete_permission = (($account->hasPermission("delete all aegir resource revisions") || $account->hasPermission('administer aegir resource entities')));

    $rows = array();

    $vids = $aegir_resource_storage->revisionIds($aegir_resource);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\aegir_resource\AegirResourceInterface $revision */
      $revision = $aegir_resource_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionAuthor(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->revision_timestamp->value, 'short');
        if ($vid != $aegir_resource->getRevisionId()) {
          $link = $this->l($date, new Url('entity.aegir_resource.revision', ['aegir_resource' => $aegir_resource->id(), 'aegir_resource_revision' => $vid], ['attributes' => ['title' => 'view revision']]));
        }
        else {
          $link = $aegir_resource->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => ['#markup' => $revision->revision_log_message->value, '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('aegir_resource.revision_revert_translation_confirm', [
                'aegir_resource' => $aegir_resource->id(),
                'aegir_resource_revision' => $vid,
                'langcode' => $langcode,
              ]) :
              Url::fromRoute('aegir_resource.revision_revert_confirm', [
                'aegir_resource' => $aegir_resource->id(),
                'aegir_resource_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('aegir_resource.revision_delete_confirm', ['aegir_resource' => $aegir_resource->id(), 'aegir_resource_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['aegir_resource_revisions_table'] = array(
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    );

    return $build;
  }

}
