<?php

namespace Drupal\aegir_resource;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Ægir Resource entities.
 *
 * @ingroup aegir_resource
 */
class AegirResourceListBuilder extends EntityListBuilder {

  use LinkGeneratorTrait;

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Ægir Resource ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\aegir_resource\Entity\AegirResource */
    $row['id'] = $entity->id();
    $row['name'] = $this->l(
      $entity->label(),
      new Url(
        'entity.aegir_resource.edit_form', array(
          'aegir_resource' => $entity->id(),
        )
      )
    );
    return $row + parent::buildRow($entity);
  }

}
