<?php

namespace Drupal\aegir_resource;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\aegir_resource\Entity\AegirResourceInterface;

/**
 * Defines the storage handler class for Ægir Resource entities.
 *
 * This extends the base storage class, adding required special handling for
 * Ægir Resource entities.
 *
 * @ingroup aegir_resource
 */
class AegirResourceStorage extends SqlContentEntityStorage implements AegirResourceStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(AegirResourceInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {aegir_resource_revision} WHERE id=:id ORDER BY vid',
      array(':id' => $entity->id())
    )->fetchCol();
  }

}
