<?php

namespace Drupal\aegir_resource;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Ægir Resource entity.
 *
 * @see \Drupal\aegir_resource\Entity\AegirResource.
 */
class AegirResourceAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\aegir_resource\Entity\AegirResourceInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished aegir resource entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published aegir resource entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit aegir resource entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete aegir resource entities');
    }

    // Unknown operation, no opinion.
    // @codeCoverageIgnoreStart
    return AccessResult::neutral();
    // @codeCoverageIgnoreEnd
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add aegir resource entities');
  }

}
