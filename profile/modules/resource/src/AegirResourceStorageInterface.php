<?php

namespace Drupal\aegir_resource;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\aegir_resource\Entity\AegirResourceInterface;

/**
 * Defines the storage handler class for Ægir Resource entities.
 *
 * This extends the base storage class, adding required special handling for
 * Ægir Resource entities.
 *
 * @ingroup aegir_resource
 */
interface AegirResourceStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Ægir Resource revision IDs for a specific Ægir Resource.
   *
   * @param \Drupal\aegir_resource\Entity\AegirResourceInterface $entity
   *   The Ægir Resource entity.
   *
   * @return int[]
   *   Ægir Resource revision IDs (in ascending order).
   */
  public function revisionIds(AegirResourceInterface $entity);

}
