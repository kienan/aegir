<?php

namespace Drupal\aegir_resource;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for aegir_resource.
 */
class AegirResourceTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
