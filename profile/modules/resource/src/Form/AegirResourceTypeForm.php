<?php

namespace Drupal\aegir_resource\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AegirResourceTypeForm.
 *
 * @package Drupal\aegir_resource\Form
 */
class AegirResourceTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $aegir_resource_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $aegir_resource_type->label(),
      '#description' => $this->t("Label for the Ægir resource type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $aegir_resource_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\aegir_resource\Entity\AegirResourceType::load',
      ],
      '#disabled' => !$aegir_resource_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $aegir_resource_type = $this->entity;
    $status = $aegir_resource_type->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Ægir resource type.', [
          '%label' => $aegir_resource_type->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Ægir resource type.', [
          '%label' => $aegir_resource_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($aegir_resource_type->urlInfo('collection'));
  }

}
