<?php

namespace Drupal\aegir_resource\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Ægir Resource entities.
 *
 * @ingroup aegir_resource
 */
class AegirResourceDeleteForm extends ContentEntityDeleteForm {


}
