<?php

namespace Drupal\aegir_resource\Tests;

use Drupal\simpletest\WebTestBase;
use Drupal\aegir_resource\Entity\AegirResource;

/**
 * Test to exercise API functions not covered by other tests.
 *
 * @group aegir
 */
class ApiTest extends WebTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'aegir_test_resource',
  ];

  public $profile = 'aegir';

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->user = $this->drupalCreateUser([], 'TestUser');
    $this->user->addRole('aegir_resource_manager');
    $this->user->save();
    $this->drupalLogin($this->user);
  }

  /**
   * Tests that the home page loads with a 200 response.
   */
  public function testApiMethods() {
    $settings = [
      // Use one of the bundles exported to the 'aegir_test_resource' module.
      'type' => 'test_resource_type_1',
      'name' => 'TEST_RESOURCE_A',
      'created' => time(),
      'user_id' => $this->rootUser->id(),
      'status' => 1,
    ];
    $resource = AegirResource::create($settings);

    $type = $resource->getType();
    $this->assertEqual($type, $settings['type'], 'Get type (bundle) of Ægir resource.');

    $name = $resource->getName();
    $this->assertEqual($name, $settings['name'], 'Get name of Ægir resource.');

    $new_name = 'TEST_RESOURCE_B';
    $resource->setName($new_name);
    $name = $resource->getName();
    $this->assertEqual($name, $new_name, 'Set name of Ægir resource.');

    $created = $resource->getCreatedTime();
    $this->assertEqual($created, $settings['created'], 'Get creation time of Ægir resource.');

    $new_created = time() - 10;
    $resource->setCreatedTime($new_created);
    $created = $resource->getCreatedTime();
    $this->assertEqual($created, $new_created, 'Set name of Ægir resource.');

    $owner_id = $resource->getOwnerId();
    $this->assertEqual($owner_id, $settings['user_id'], 'Get owner of Ægir resource.');

    $new_user = $this->drupalCreateUser([], 'TestUser2');
    $resource->setOwner($new_user);
    $owner_id = $resource->getOwnerId();
    $this->assertEqual($owner_id, $new_user->id(), 'Set owner of Ægir resource.');

    $published = $resource->isPublished();
    $this->assertEqual($published, $settings['status'], 'Get published status of Ægir resource.');

    $unpublished = FALSE;
    $resource->setPublished($unpublished);
    $published = $resource->isPublished();
    $this->assertEqual($published, $unpublished, 'Set published status of Ægir resource.');

    $resource->save();
    $this->drupalGet('admin/aegir/resources/' . $resource->id());
    $this->assertResponse(200);
    $this->assertText($new_name, 'User with "Resource manager" role can access un-published Ægir resources.');

  }

}
