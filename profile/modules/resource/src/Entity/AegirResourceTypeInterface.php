<?php

namespace Drupal\aegir_resource\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Ægir resource type entities.
 */
interface AegirResourceTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
