<?php

namespace Drupal\aegir_resource\Entity;

use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Ægir resource entities.
 *
 * @ingroup aegir_resource
 */
interface AegirResourceInterface extends RevisionableInterface, EntityChangedInterface, EntityOwnerInterface {

  /* Add get/set methods for your configuration properties here. */

  /**
   * Gets the Ægir resource type.
   *
   * @return string
   *   The Ægir resource type.
   */
  public function getType();

  /**
   * Gets the Ægir resource name.
   *
   * @return string
   *   Name of the Ægir resource.
   */
  public function getName();

  /**
   * Sets the Ægir resource name.
   *
   * @param string $name
   *   The Ægir resource name.
   *
   * @return \Drupal\aegir_resource\Entity\AegirResourceInterface
   *   The called Ægir resource entity.
   */
  public function setName($name);

  /**
   * Gets the Ægir resource creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Ægir resource.
   */
  public function getCreatedTime();

  /**
   * Sets the Ægir resource creation timestamp.
   *
   * @param int $timestamp
   *   The Ægir resource creation timestamp.
   *
   * @return \Drupal\aegir_resource\Entity\AegirResourceInterface
   *   The called Ægir resource entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Ægir resource published status indicator.
   *
   * Unpublished Ægir resource are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Ægir resource is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Ægir resource.
   *
   * @param bool $published
   *   TRUE to set this Ægir resource to published, FALSE to set it to
   *   unpublished.
   *
   * @return \Drupal\aegir_resource\Entity\AegirResourceInterface
   *   The called Ægir resource entity.
   */
  public function setPublished($published);

  /**
   * Gets the Ægir resource revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Ægir resource revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\aegir_resource\Entity\AegirResourceInterface
   *   The called Ægir resource entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Ægir resource revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionAuthor();

  /**
   * Sets the Ægir resource revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\aegir_resource\Entity\AegirResourceInterface
   *   The called Ægir resource entity.
   */
  public function setRevisionAuthorId($uid);

}
