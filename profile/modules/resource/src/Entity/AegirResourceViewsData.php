<?php

namespace Drupal\aegir_resource\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Ægir Resource entities.
 */
class AegirResourceViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['aegir_resource']['table']['base'] = array(
      'field' => 'id',
      'title' => $this->t('Ægir Resource'),
      'help' => $this->t('The Ægir Resource ID.'),
    );

    return $data;
  }

}
