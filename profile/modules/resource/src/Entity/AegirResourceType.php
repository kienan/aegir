<?php

namespace Drupal\aegir_resource\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Ægir resource type entity.
 *
 * @ConfigEntityType(
 *   id = "aegir_resource_type",
 *   label = @Translation("Resource type"),
 *   handlers = {
 *     "list_builder" = "Drupal\aegir_resource\AegirResourceTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\aegir_resource\Form\AegirResourceTypeForm",
 *       "edit" = "Drupal\aegir_resource\Form\AegirResourceTypeForm",
 *       "delete" = "Drupal\aegir_resource\Form\AegirResourceTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\aegir_resource\AegirResourceTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "aegir_resource_type",
 *   admin_permission = "administer aegir resource types",
 *   bundle_of = "aegir_resource",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/aegir/resources/types/{aegir_resource_type}",
 *     "add-form" = "/admin/aegir/resources/types/add",
 *     "edit-form" = "/admin/aegir/resources/types/{aegir_resource_type}/edit",
 *     "delete-form" = "/admin/aegir/resources/types/{aegir_resource_type}/delete",
 *     "collection" = "/admin/aegir/resources/types"
 *   }
 * )
 */
class AegirResourceType extends ConfigEntityBundleBase implements AegirResourceTypeInterface {

  /**
   * The Ægir resource type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Ægir resource type label.
   *
   * @var string
   */
  protected $label;

}
