@resources @revisions @api
Feature: Create and manage Aegir resources
  In order to define resources that can be combined into applications,
  as a resource manager,
  I want to be able to manage Aegir resource entities.

  Background:
    Given I run "drush @aegir -y en aegir_test_resource"
      And I am logged in as a "Resource manager"
      And I am on "admin/aegir/resources"

  Scenario: Create a resource for later scenarios
    Given I click "Add Ægir resource"
      And I click "TEST_RESOURCE_TYPE_1" in the "content" region
      And I fill in "Name" with "TEST_RESOURCE_B"
      And I press the "Save" button
     Then I should see the success message "Created the TEST_RESOURCE_B Ægir resource."
      And I should see the link "Revisions" in the "tabs" region

  Scenario: Update the resource to create some revisions
    Given I should see the link "TEST_RESOURCE_B" in the "content" region
      And I click "TEST_RESOURCE_B"
     When I check the box "Create new revision"
      And for "Revision log message" I enter "First test log message."
      And I press the "Save" button
     Then I should see the success message "Saved the TEST_RESOURCE_B Ægir resource."
     When I click "Edit" in the "tabs" region
      And I check the box "Create new revision"
      And for "Revision log message" I enter "Second test log message."
      And I press the "Save" button
      And I click "Revisions"
     Then I should see the heading "Revisions for TEST_RESOURCE_B" in the "header" region
      And I should see the text "First test log message."
      And I should see the text "Second test log message."

  Scenario: View a resource revision
    Given I click "TEST_RESOURCE_B"
      And I click "Revisions"
     When I click "view revision" in the "First test log message." row
     Then I should see "Revision of TEST_RESOURCE_B from" in the "header" region

  Scenario: Revert a resource revision
    Given I click "TEST_RESOURCE_B"
      And I click "Revisions"
     When I click "Revert" in the "First test log message." row
     Then I should see the text "Are you sure you want to revert to the revision from" in the "header" region
     When I press "Revert"
     Then I should see the success message "Ægir Resource TEST_RESOURCE_B has been reverted to the revision from"


