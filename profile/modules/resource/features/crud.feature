@resources @entities @api
Feature: Create and manage Aegir resources
  In order to define resources that can be combined into applications,
  as a resource manager,
  I want to be able to manage Aegir resource entities.

  Background:
    Given I run "drush @aegir -y en aegir_test_resource"
      And I am logged in as a "Resource manager"
      And I am on "admin/aegir/resources"

  Scenario: Create resources
     When I click "Add Ægir resource"
     Then I should be on "admin/aegir/resources/add"
      And I should see the heading "Add resource" in the "header" region
      And I should see the link "TEST_RESOURCE_TYPE_1"
      And I should see the link "TEST_RESOURCE_TYPE_2"
     When I click "TEST_RESOURCE_TYPE_1" in the "content" region
     Then I should be on "admin/aegir/resource/add/test_resource_type_1"
     When I fill in "Name" with "TEST_RESOURCE_A"
      And I press the "Save" button
     Then I should see the success message "Created the TEST_RESOURCE_A Ægir resource."
      And I should see the link "View" in the "tabs" region
      And I should see the link "Edit" in the "tabs" region
      And I should see the link "Revisions" in the "tabs" region
      And I should see the link "Delete" in the "tabs" region


  Scenario: Update resources
    Given I should see the link "TEST_RESOURCE_A" in the "content" region
      And I click "TEST_RESOURCE_A"
     When I check the box "Create new revision"
      And for "Revision log message" I enter "Test log message."
      And I press the "Save" button
     Then I should see the success message "Saved the TEST_RESOURCE_A Ægir resource."

  Scenario: Delete resources
    Given I click "TEST_RESOURCE_A" in the "content" region
     Then I should see the heading "Edit TEST_RESOURCE_A" in the "header" region
     When I click "Delete" in the "content" region
     Then I should see the heading "Are you sure you want to delete the resource TEST_RESOURCE_A?" in the "header" region
     When I press the "Delete" button
     Then I should see the success message "The resource TEST_RESOURCE_A has been deleted."
      And I should be on "admin/aegir/resources"
      And I should not see the link "TEST_RESOURCE_A"

