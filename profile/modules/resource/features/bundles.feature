@resources @types @api
Feature: Create and manage Aegir resource types
  In order to define resource types that can be combined into applications,
  as a resource manager,
  I want to be able to manage Aegir resource bundles.

  Background:
    Given I am logged in as a "Resource manager"
      And I am on "admin/aegir/resources/types"


  Scenario: Create resource types
     When I click "Add Ægir resource type"
      And I fill in "Label" with "TEST_RESOURCE_TYPE_NEW"
      And I fill in "Machine-readable name" with "test_resource_type_new"
      And I press the "Save" button
     Then I should be on "admin/aegir/resources/types"
      And I should see the success message "Created the TEST_RESOURCE_TYPE_NEW Ægir resource type."

  Scenario: Update resource types
     When I click "TEST_RESOURCE_TYPE_NEW"
     Then I should see the heading "Edit resource type" in the "header" region
     When I fill in "Label" with "TEST_RESOURCE_TYPE_CHANGED"
      And I press the "Save" button
     Then I should be on "admin/aegir/resources/types"
     Then I should see the success message "Saved the TEST_RESOURCE_TYPE_CHANGED Ægir resource type."

  Scenario: Delete resource types
     Then I should see the link "TEST_RESOURCE_TYPE_CHANGED"
     When I click "TEST_RESOURCE_TYPE_CHANGED"
     Then I should see the heading "Edit resource type" in the "header" region
     When I click "Delete" in the "content" region
     Then I should see the heading "Are you sure you want to delete TEST_RESOURCE_TYPE_CHANGED?" in the "header" region
     When I press the "Delete" button
     Then I should be on "admin/aegir/resources/types"
     Then I should see the success message "The Ægir resource type TEST_RESOURCE_TYPE_CHANGED has been deleted."

