@resources @access @api
Feature: Access to Aegir resources and types
  In order to define resources that can be combined into applications,
  as a resource manager,
  I need to be able to access Aegir resource entities and types.

  Scenario: Anonymous users should not have access to resources configuration.
    Given I am not logged in
     When I am on "admin/aegir"
     Then I should see "Access denied"

  Scenario: Authenticated users should not have access to resources configuration.
    Given I am logged in as an "Authenticated user"
     When I am on "admin/aegir"
     Then I should not see the link "Resources"

  Scenario: Resource managers should have access to resources configuration.
    Given I am logged in as a "Resource manager"
     When I am on "admin/aegir"
     Then I should see the link "Resources"
      And I should see the text "Ægir resource entities and bundles"
     When I click "Resources"
     Then I should be on "admin/aegir/resources"
      And I should see the heading "Resources" in the "header" region
      And I should see the link "Add Ægir resource"
      And I should see the link "Resource types"
     When I click "Resource types"
     Then I should be on "admin/aegir/resources/types"
      And I should see the heading "Resource types" in the "header" region
      And I should see the link "Add Ægir resource type"

