PROJECT_TYPE ?= profile
PROJECT_NAME ?= aegir
include .mk/GNUmakefile

.DEFAULT_GOAL = help-aegir

# Determine whether we're in a dev/test VM or not, so that we run our command
# in the proper environment.
CURRENT_USER = $(shell whoami)
VM_USERS = root vagrant
ifneq ($(filter $(CURRENT_USER),$(VM_USERS)),)
    # We're in a VM environment, so just run the command directly.
    run = eval
else
    # We're on the local host, and should therefore run the command in Vagrant.
    # @TODO Check for VB/Docker and switch command further as appropriate.
    run = vagrant ssh -c
endif

CI_PROJECT_DIR ?= /vagrant
AEGIR_PLATFORM_PATH ?= /var/aegir/platforms/aegir
ifdef TAGS
    BEHAT_TAGS = --tags='$(TAGS)'
endif

help-aegir:
	@echo "help-aegir"
	@echo "  Print this help message."
	@echo "run-simpletest"
	@echo "  Run Simpletest-based tests."
	@echo "run-behat"
	@echo "  Run Behat-based tests."
	@echo "coverage-init"
	@echo "  Initialize coverage (i.e., install xdebug, etc.)."
	@echo "run-coverage"
	@echo "  Run tests to gather code-coverage data."
	@echo "coverage-text"
	@echo "  Generate a text code-coverage report (from existing coverage data.)"
	@echo "coverage-html"
	@echo "  Generate an HTML code-coverage report (from existing coverage data.)"

simpletest-init:
	@$(run) "sudo -u aegir --login drush @aegir -y en simpletest"

# N.B. xdebug-2.4.0 seg faults under Apache, hence 2.4.1+.
xdebug-init:
	@$(run) "sudo /bin/bash -c \"echo 'deb http://us.archive.ubuntu.com/ubuntu/ zesty universe' > /etc/apt/sources.list.d/zesty-universe.list\""
	@$(run) "sudo apt-get update -qq"
	@$(run) "sudo apt-get install -y php-xdebug -t zesty > /dev/null"
	@$(run) "sudo apache2ctl restart"

coverage-init: xdebug-init
	@$(run) "sudo /bin/bash -c 'echo \"memory_limit = 1024M\" > /etc/php/7.0/cli/conf.d/99-memory.ini'"
	@$(run) "sudo /bin/bash -c 'echo \"memory_limit = 1024M\" > /etc/php/7.0/fpm/conf.d/99-memory.ini'"
	@$(run) "sudo /bin/bash -c 'echo \"memory_limit = 1024M\" > /etc/php/7.0/apache2/conf.d/99-memory.ini'"
	@$(run) "sudo /bin/bash -c 'echo \"SetEnv CI_PROJECT_DIR $(CI_PROJECT_DIR)\" > /etc/apache2/conf-enabled/env-vars.conf'"
	@$(run) "sudo /bin/bash -c 'echo \"SetEnv AEGIR_PLATFORM_PATH $(AEGIR_PLATFORM_PATH)\" >> /etc/apache2/conf-enabled/env-vars.conf'"
	@$(run) "sudo apache2ctl restart"

run-simpletest: simpletest-init
	@$(run) "sudo -u www-data CI_PROJECT_DIR=$(CI_PROJECT_DIR) php $(AEGIR_PLATFORM_PATH)/core/scripts/run-tests.sh --non-html --color --verbose --url http://aegir.dev aegir"

run-behat:
	@$(run) "sudo -u aegir --login /bin/bash -c 'cd $(CI_PROJECT_DIR)/tests && CI_PROJECT_DIR=$(CI_PROJECT_DIR) behat --stop-on-failure --colors $(BEHAT_TAGS)'"

wip: run-behat-wip

run-behat-wip:
	@$(run) "sudo -u aegir --login /bin/bash -c 'cd $(CI_PROJECT_DIR)/tests && CI_PROJECT_DIR=$(CI_PROJECT_DIR) behat --colors --tags=\"@wip&&~@disabled\"'"

run-tests: run-simpletest run-behat

coverage-dir-writable:
	@chmod 777 build/coverage/ -R

coverage-clean-php: coverage-dir-writable
	@rm -rf build/coverage/php/*.cov

coverage-clean-html: coverage-dir-writable
	@rm -rf build/coverage/html/*/

generate-coverage-text:
	@$(run) "sudo -u aegir --login phpcov merge $(CI_PROJECT_DIR)/build/coverage/php --text=1"

generate-coverage-html: coverage-clean-html
	@$(run) "sudo -u aegir --login phpcov merge $(CI_PROJECT_DIR)/build/coverage/php/ --html $(CI_PROJECT_DIR)/build/coverage/html/merged"
	@echo "Coverage report URL: file://$(shell pwd)/build/coverage/html/merged/index.html"

cover-simpletest: coverage-clean-php add-coverage-prepend run-simpletest remove-coverage-prepend

cover-behat: coverage-clean-php add-coverage-prepend run-behat remove-coverage-prepend

cover-all: coverage-clean-php add-coverage-prepend run-tests remove-coverage-prepend generate-coverage-text generate-coverage-html

add-coverage-prepend:
	@$(run) "sudo /bin/bash -c 'echo \"auto_prepend_file=$(CI_PROJECT_DIR)/build/coverage/prepend.php\" > /etc/php/7.0/apache2/conf.d/98-coverage.ini'"
	@$(run) "sudo /bin/bash -c 'echo \"auto_prepend_file=$(CI_PROJECT_DIR)/build/coverage/prepend.php\" > /etc/php/7.0/fpm/conf.d/98-coverage.ini'"
	@$(run) "sudo /bin/bash -c 'echo \"auto_prepend_file=$(CI_PROJECT_DIR)/build/coverage/prepend.php\" > /etc/php/7.0/cli/conf.d/98-coverage.ini'"
	@$(run) "sudo apache2ctl restart"

remove-coverage-prepend:
	@$(run) "sudo rm /etc/php/7.0/apache2/conf.d/98-coverage.ini"
	@$(run) "sudo rm /etc/php/7.0/fpm/conf.d/98-coverage.ini"
	@$(run) "sudo rm /etc/php/7.0/cli/conf.d/98-coverage.ini"
	@$(run) "sudo apache2ctl restart"

lint-init:
# Register Drupal coding standards
	@$(run) "sudo -u aegir --login /bin/bash -c 'phpcs --config-set installed_paths $(AEGIR_PLATFORM_PATH)/modules/coder/coder_sniffer'"
	@$(run) "sudo -u aegir --login /bin/bash -c 'phpcs -i'"

lint: lint-init
# Analyze code for Drupal coding standards violations.
	@$(run) "sudo -u aegir --login /bin/bash -c 'phpcs --colors --standard=Drupal,DrupalPractice --extensions=php,module,install,inc -v $(AEGIR_PLATFORM_PATH)/profiles/aegir/'"

re-install:
	@$(run) "sudo -u aegir --login /bin/bash -c 'cd $(AEGIR_PLATFORM_PATH); drush site-install -y --db-url=mysql://root:root@localhost/aegir0 --account-name=admin --account-pass=pwd --site-name=Ægir'"

re-dev: re-install
	@$(run) "sudo -u aegir --login drush @aegir -y -q en aegir_devel aegir_test_resource"
	@$(run) "sudo -u aegir --login drush @aegir uli"

re-up:
	@vagrant destroy -f
	@vagrant up

logs:
	@$(run) "sudo tail /var/log/apache2/error.log"
