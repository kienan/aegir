<?php

# TODO: Make Drumkit CLI context available for use via Composer.
include dirname(dirname(dirname(dirname(__FILE__)))) . "/.mk/features/bootstrap/DrumkitContext.php";

use Behat\Behat\Hook\Scope\AfterFeatureScope;

class FeatureContext extends DrumkitContext {

  /**
   * @AfterFeature
   */
  public static function cleanEntities(AfterFeatureScope $scope) {
    # List the entity and bundle types to clean up after.
    $types = [
      'resources' => [
        'bundle' => 'aegir_resource_type',
        'entity' => 'aegir_resource',
        'test_modules' => [
          'aegir_test_resource',
          'content_translation',
          'language',
        ],
      ],
    ];

    # All test entities and bundles should be prefixed for easy identification.
    $prefix = 'TEST_';

    foreach ($scope->getFeature()->getTags() as $tag) {
      if (array_key_exists($tag, $types)) {
        print("Cleaning up test '{$tag}' entities and bundles.\n");
        $data = $types[$tag];
        $query = \Drupal::entityQuery($data['bundle']);
        $bundles = $query
          ->condition('label', $prefix, 'STARTS_WITH')
          ->execute();
        foreach ($bundles as $bundle_label) {
          $query = \Drupal::entityQuery($data['entity']);
          $entity_ids = $query
            ->condition('type', $bundle_label)
            ->condition('name', $prefix, 'STARTS_WITH')
            ->execute();
          $entities = \Drupal::entityTypeManager()
            ->getStorage($data['entity'])
            ->loadMultiple($entity_ids);
          foreach ($entities as $id => $entity) {
            print('  Deleting ' . $entity->label() . ' ' . $entity->getEntityType()->getLabel() . ' (bundle: ' . $bundle_label . ")\n");
            $entity->delete();
          }
          $bundle = \Drupal::entityTypeManager()
            ->getStorage($data['bundle'])
            ->load($bundle_label);
          print('  Deleting ' . $bundle_label . ' ' . $bundle->getEntityType()->getLabel() . "\n");
          $bundle->delete();
        }
        print('Disabling test module(s): ' . implode(',', $data['test_modules']) . "\n");
        \Drupal::service('module_installer')->uninstall($data['test_modules']);
      }
    }
  }

}
