Feature: Provide a default theme.
  In order to develop a usable interface,
  as a developer,
  I want to be able to use a flexible default theme.

  Scenario: Use a popular base theme.
    Given I run "drush @aegir pmi eldir"
     Then I should get:
     """
     Type        :  theme
     Title       :  Eldir
     Status      :  enabled
     Base        :  Bootstrap
     """
